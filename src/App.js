import React, { Component } from 'react';
import './App.css';
import {Headers} from './components/header.js'
import Content from './components/content.js'
import Loaders from './components/loader'



class App extends Component {
  state = {
    loading: true
  }
  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 1500); // simulates an async action, and hides the spinner
  }
  render() {
    const { loading } = this.state;
    
    if(loading) {  
      return <Loaders/>  
      
    }
    
    return (
      <div className ='main' >
        <div className='top' >
            <Headers />
            {console.log(this.state.title)}
          </div>
        <div className='content'>
          <Content title={this.state.title} />
        </div>
        <div>
          <p className='footer'>© 2018 Copyright Discovery News Network. All rights reserved.</p>
        </div>
      </div>
    );
  }
}

export default App;
