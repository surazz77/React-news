import React from 'react'
import { Dimmer, Loader} from 'semantic-ui-react'

const Loaders = () => (
    <div style={{height:'100%'}}>
            <Dimmer active>
                <Loader size='small'>Loading</Loader>
            </Dimmer>

  </div>
)

export default Loaders