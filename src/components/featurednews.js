import React from 'react'
import { List,Card,Image } from 'semantic-ui-react'
import '../App.css' 

class FeaturedNews extends React.Component{
render(){
  return(
      <div>
    
    <List relaxed>
    <Card style={{width:'100%'}}>
    <Image src= {this.props.image}/>
    <Card.Content>      
      <Card.Header as = 'h1' >{this.props.title}</Card.Header>
    </Card.Content>
  </Card>
  
  </List>
          </div>
  )}}
  export default FeaturedNews