import React from 'react'
import { Card, Icon,Image } from 'semantic-ui-react'
import '../App.css' 


class NewsCard extends React.Component{
render(){
  return(
  <div >

    
    <Card className='maincontent' style={{width:'100%', marginBottom:'40px'}}>
    
        <Card.Content >
            <Card.Header as ='h2'>source :{this.props.author}
              <Card.Meta>{this.props.published}
              </Card.Meta>
            </Card.Header>

            <Card.Header as = 'h1' style={{fontSize:'50px'}}>
                {this.props.title}
              </Card.Header>
        </Card.Content>

        <Image src={this.props.image}/>
        <Card.Content>
            <Card.Description>
              {this.props.description}
            </Card.Description>
        </Card.Content>
        <Card.Content >
          <div style={{float:'right'}}>
            <Icon name='facebook f' size='large' />
            <Icon name='twitter' size='large' />
            <Icon name='linkedin' size='large'/>
            <Icon name='paperclip' size='large'/>
          </div>
        </Card.Content>
  </Card>
  
  </div>

)
}}

export default NewsCard