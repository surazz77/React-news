import React from 'react'
import '../App.css';
import FeaturedNews from './featurednews.js'
import LatestNews from './latestnews.js'
import { Segment,Card,Image } from 'semantic-ui-react'
import withData from './withData'


class SideBar extends React.Component{
 
  render(){
    return(
    <div>
         <p className='stories'>LATEST STORIES</p>
    <Segment>

      <div>
          {
            this.props.data &&
            this.props.data.map((data,index)=>{
              return <LatestNews key={index} title={data.title} description={data.description} image={data.urlToImage} author={data.author} published = {data.publishedAt}/>
          })}

      </div>
          
  </Segment>
    <Card>
      <Image src = 'http://ratopati.prixa.net/media/ime-pay-263x150.gif' />
    </Card>
    <Card>
      <Image src = 'http://ratopati.prixa.net/media/280X150-PRABHU-HOME-LOAN.gif' />
    </Card>
    <Card>
      <Image src = 'http://ratopati.prixa.net/media/280-x-150_MBL-Corporate.gif' />
    </Card>
  <p className='stories'>FEATURED STORIES</p>
  <Segment>
    <div>
                      
        {this.props.data &&
          this.props.data.map((data,index)=>{
            return <FeaturedNews key={index} title={data.title} description={data.description} image={data.urlToImage} author={data.author} published = {data.publishedAt}/>

        })}

    </div>
  </Segment>



 
  </div>
)
  }}

  export default withData(SideBar)