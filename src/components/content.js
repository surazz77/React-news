import React from "react"
import NewsCard from './newscontent.js'
import SideBar from './sidebar.js'
import withData from './withData'
import '../App.css'


class Content extends React.Component{
    render(){
        return(
            <div className='container'>
                    <div className='side'><SideBar /></div>                
                <div className='middle'>
                <p className='stories'>TOP STORIES</p>

                    { this.props.data &&
                        this.props.data.map((data, index) => {
                        return <NewsCard key={index} title={data.title} description={data.description}  image={data.urlToImage} author={data.author} published = {data.publishedAt}/>

                    })}
                </div>
            </div>
        );
    }
}

export default withData(Content)