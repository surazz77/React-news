import React, { Component } from 'react'
import { Input, Menu,Dropdown } from 'semantic-ui-react'
import '../App.css' 
import { Image } from 'semantic-ui-react'
export class Headers extends Component {

  state = { activeItem: 'home' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })
  
  render() {
    const options = [
      { key: 1, text: 'Sports', value: 1 },
      { key: 2, text: 'football', value: 2 },
      { key: 3, text: 'Cricket', value: 3 },
    ]
    
    const { activeItem } = this.state
    // var a=U
    return (
      <div className='head'>
  
      <Menu secondary className='nav-bar'>
        
        <Menu.Item>
          <Image 
            src=' https://jcss-cdn.kantipurdaily.com/kantipurdaily/images/logo.png' size='small'/>
          </Menu.Item>
        
        <Menu.Item  
          name='home'size='huge' active={activeItem === 'home'} onClick={this.handleItemClick} 
        />
        
        <Menu.Item
        
          name='Pardesh>'
          active={activeItem === 'pardesh'}
          onClick={this.handleItemClick}
        />
        
        <Menu.Item
          name='National'
          active={activeItem === 'National'}
          onClick={this.handleItemClick}
        />
      
      <Menu.Item>
        <Dropdown as='a'
          inline
          options={options}
          defaultValue={options[0].value}
          active={(activeItem === 'Sports').toString()}
            onClick={this.handleItemClick}
          simple item
        />
      </Menu.Item>
         <Menu.Item
            name='Fashion'
            
            active={activeItem === 'Fashion'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='International'
            active={activeItem === 'International'}
            onClick={this.handleItemClick}
          />

        <Menu.Menu position='right'>
          <Menu.Item>
            <Input icon='search' placeholder='Search...' />
          </Menu.Item>
          
        </Menu.Menu>
      </Menu>

    </div>
    )
  }
}