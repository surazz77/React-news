import React from 'react'
import { List } from 'semantic-ui-react'
import '../App.css' 

class LatestNews extends React.Component{
render(){
  return(
      <div>
           <List divided relaxed  className ='sidenews' >
    <List.Item >
      <List.Content>
        <List.Header as='a' >{this.props.title}</List.Header>
        <List.Description style={{ paddingTop: '10px', paddingBottom:'20px' }} as='a'>10 mins ago/events</List.Description>
      </List.Content>
    </List.Item>
    
  </List>
    
    
          </div>
  )}}
  export default LatestNews