import React from "react"
import http from '../utils/http'

export default (WrappedComponent) => class extends React.Component {
	state = {}

	async componentDidMount() {
		const response = await http.get('/top-headlines?country=us&apiKey=e545277819c84cda801da80b8765bc1c');
		const data = response.data.articles;

		this.setState({data})
	}

	render() {
		return (
			<WrappedComponent data={this.state.data} {...this.props} />
		)
	}
}